﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {


	public GameObject _avoidClicks;
	public int current_clicked_cards;

    private GameObject selected_element_1;
    private GameObject selected_element_2;
    private GameObject new_element;
    private GameObject dupeCard;
    private GameObject dupeComb;
    private bool       doubleCard = false;
    private bool       doubleComb = false;

    public GameObject CardPrefab;
    

    [Header("Timings")]
    public int   RitualDuration = 3   ;
    public float CameraDelay    = 0.3f;
    public float ResetDelay     = 1.5f;

	// Use this for initialization
	void Start () 
    {
		_avoidClicks.SetActive (false);
        current_clicked_cards = 0;
        InitializeBasicElements();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    private void InitializeBasicElements()
    {


        GameObject Fire  = Instantiate(CardPrefab); Fire.GetComponent<CardDetails>().CardName   = "FIRE" ; Fire.name    = "FIRE"    ;
        GameObject Water = Instantiate(CardPrefab); Water.GetComponent<CardDetails>().CardName  = "WATER"; Water.name   = "WATER"   ;
        GameObject Earth = Instantiate(CardPrefab); Earth.GetComponent<CardDetails>().CardName  = "EARTH"; Earth.name   = "EARTH"   ;
        GameObject Air   = Instantiate(CardPrefab); Air.GetComponent<CardDetails>().CardName    = "AIR"  ; Air.name     = "AIR"     ;

        ////ADD ALLOCATOR LOGIC
        //TEMP LOGIC:
        Fire.GetComponent<CardDetails>().ShelfPosition = GetComponent<CardAllocator>().addCard("FIRE");
        Fire.GetComponent<CardMovement>().moveTo(Fire.GetComponent<CardDetails>().ShelfPosition, -90);
        Water.GetComponent<CardDetails>().ShelfPosition = GetComponent<CardAllocator>().addCard("WATER");
        Water.GetComponent<CardMovement>().moveTo(Water.GetComponent<CardDetails>().ShelfPosition, -90);
        Earth.GetComponent<CardDetails>().ShelfPosition = GetComponent<CardAllocator>().addCard("EARTH");
        Earth.GetComponent<CardMovement>().moveTo(Earth.GetComponent<CardDetails>().ShelfPosition, -90);
        Air.GetComponent<CardDetails>().ShelfPosition = GetComponent<CardAllocator>().addCard("AIR");
        Air.GetComponent<CardMovement>().moveTo(Air.GetComponent<CardDetails>().ShelfPosition, -90);
    }

    public void CardClicked(GameObject SelectedCard)
    {
        current_clicked_cards++;

        if (current_clicked_cards == 1)
        {
            Debug.Log("GAME_MANAGER: First Card Clicked (" + SelectedCard.GetComponent<CardDetails>().CardName + ").");
            selected_element_1 = SelectedCard;
            selected_element_1.GetComponent<CardHover>()._state = 1;
        }
        else if (current_clicked_cards == 2)
        {
			_avoidClicks.SetActive (true);
            Debug.Log("GAME_MANAGER: Second Card Clicked (" + SelectedCard.GetComponent<CardDetails>().CardName + ").");
            selected_element_2 = SelectedCard;
            selected_element_2.GetComponent<CardHover>()._state = 1;


			Debug.Log ("GAME_MANAGER: Is valid? " + IsValidSelection ());
            //PERFORM ACTION HERE
			if (IsValidSelection ()) 
            {
				validCombinationSelected ();
			} 
            else 
            {
				notValidCombinationSelected ();
			}

            current_clicked_cards = 0;
            Debug.Log("GAME_MANAGER: Card Counter Reset.");
        }
        else
        {
			current_clicked_cards = 0;
            Debug.Log("GAME_MANAGER: ERROR: Card Counter Fail (Cards = " + current_clicked_cards + ").");

        }
    }

    private bool DCTip = true;
	private void validCombinationSelected(){

        if (selected_element_1 == selected_element_2)
        {
            dupeCard = Instantiate(selected_element_2);
            selected_element_1 = dupeCard;
            doubleCard = true;

            if (DCTip)
            {
                DCTip = false;
                GameObject.Find("TextDoubleClickTiptxt").GetComponent<Text>().text = "";
            }

        }
            
		AudioClip clip = Resources.Load<AudioClip> ("Audio/ValidCombination");
		if(clip != null){
			GetComponent <AudioSource> ().clip = clip;
			GetComponent <AudioSource> ().Play();
		} else {
			Debug.Log ("AudioNotFound: " + "Audio/ValidCombination");
		}
        
        // move Camera Down
		CameraController cam = Camera.main.GetComponent<CameraController> ();

        Vector3 destiny1 = GameObject.FindGameObjectWithTag("CardPosition1").transform.position;
        Vector3 destiny2 = GameObject.FindGameObjectWithTag("CardPosition2").transform.position;
        selected_element_1.GetComponent<CardMovement>().moveTo(destiny1, -25);
        selected_element_2.GetComponent<CardMovement>().moveTo(destiny2, -25);


        if (cam != null)
            cam.Invoke("moveDown", CameraDelay);

        string card_name_1 = selected_element_1.GetComponent<CardDetails>().CardName;
        string card_name_2 = selected_element_2.GetComponent<CardDetails>().CardName;

		// create a new card with the data
		// Start Flip Card
        ComboRitual();

	} // TwoCardClickSucess

    private void ComboRitual()
    {
        //this function will do the combination of two cards, then call finishFlipCard

        string card_name_1 = selected_element_1.GetComponent<CardDetails>().CardName;
        string card_name_2 = selected_element_2.GetComponent<CardDetails>().CardName;
        CardInfo NewCardInfo = GetComponent<CardDatabase>().getCardInfo(card_name_1, card_name_2);
		Vector3 newElPos = GameObject.FindGameObjectWithTag("CardPosition3").transform.position;
		Vector3 newCrPos = GetComponent<CardAllocator>().addCard(NewCardInfo.name);

		new_element = Instantiate(CardPrefab, newElPos, Quaternion.Euler(-25, 0, 180)) as GameObject;

        new_element.GetComponent<CardHover>()._state = 1;
        new_element.GetComponent<CardDetails>().SetInfo(NewCardInfo);
		NewCardInfo.unlock = true;
        new_element.name = NewCardInfo.name;
        new_element.GetComponent<CardDetails>().ShelfPosition = newCrPos;

		if (newCrPos.x == 0) {
			dupeComb = new_element;
            dupeComb.GetComponent<CardDetails>().ShelfPosition = GameObject.Find(new_element.name).GetComponent<CardDetails>().ShelfPosition;
            dupeComb.GetComponent<CardDetails>().ShelfPosition.z += 0.5f;
            dupeComb.GetComponent<CardDetails>().ShelfPosition.y += 0.5f;
            doubleComb = true;
		} 


        //Vector3 destiny3 = GameObject.FindGameObjectWithTag("CardPosition3").transform.position;
        //new_element.GetComponent<CardMovement>().moveTo(destiny3, -25);

        Invoke("ComboTextAppear", RitualDuration / 2);
		Invoke("startFlipCard", RitualDuration );
    }

    private void ComboTextAppear()
    {
        string card_name_1 = selected_element_1.GetComponent<CardDetails>().CardName;
        string card_name_2 = selected_element_2.GetComponent<CardDetails>().CardName;
        GameObject.FindWithTag("GUIManager").GetComponent<GUIManager>().addCraftText(card_name_1, card_name_2);
    }
	private void startFlipCard(){

		new_element.GetComponent<Animation> ().Play ();
		Invoke("finishFlipCard", RitualDuration);
		if (!doubleComb) {
			GameObject.FindWithTag ("GUIManager").GetComponent<GUIManager> ().showNewElementMessage ();
			AudioClip clip = Resources.Load<AudioClip> ("Audio/newElement");
			if(clip != null){
				GetComponent <AudioSource> ().clip = clip;
				GetComponent <AudioSource> ().Play();
			} else {
				Debug.Log ("AudioNotFound: " + "Audio/newElement");
			}

			clip = Resources.Load<AudioClip> ("Audio/"+new_element.GetComponent<CardDetails>().CardName);
			if(clip != null){
				GetComponent <AudioSource> ().clip = clip;
				GetComponent <AudioSource> ().Play();
			}else {
				Debug.Log ("AudioNotFound: " + "Audio/"+new_element.GetComponent<CardDetails>().CardName);
			}

		}
		GameObject.FindWithTag ("GUIManager").GetComponent<GUIManager> ().addTopText(new_element.GetComponent<CardDetails>().CardName);

		AudioClip clip2 = Resources.Load<AudioClip> ("Audio/startFlip");
		if(clip2 != null){
			GetComponent <AudioSource> ().clip = clip2;
			GetComponent <AudioSource> ().Play();
		} else {
			Debug.Log ("AudioNotFound: " + "Audio/startFlip");
		}
	}

	private void finishFlipCard()
    {
		// move camera up, an move all card on top
		CameraController cam = Camera.main.GetComponent<CameraController> ();

        selected_element_1.GetComponent<CardMovement>().moveTo(selected_element_1.GetComponent<CardDetails>().ShelfPosition, -90);
        selected_element_2.GetComponent<CardMovement>().moveTo(selected_element_2.GetComponent<CardDetails>().ShelfPosition, -90);
        new_element.GetComponent<CardMovement>().moveTo(new_element.GetComponent<CardDetails>().ShelfPosition, -90);

		GameObject.FindWithTag ("GUIManager").GetComponent<GUIManager> ().hideText();

        if (cam != null)
            cam.Invoke("moveUp", CameraDelay);


        selected_element_2.GetComponent<CardHover>().CardBack();
        new_element.GetComponent<CardHover>().CardBack();
        Invoke("resetStates", ResetDelay);

		// move the three card ( 2 selected and the new one ) to the allocator pos
	} // finishFlipCard

    private void resetStates()
    {
		
        if (doubleCard) Destroy(dupeCard);
        if (doubleComb) Destroy(dupeComb);
        selected_element_1.GetComponent<CardHover>()._state = 0;
		selected_element_1.GetComponent<CardClickable> ().setHalo (false);
        selected_element_2.GetComponent<CardHover>()._state = 0;
		selected_element_2.GetComponent<CardClickable> ().setHalo (false);
		if(new_element != null)
        	new_element.GetComponent<CardHover>()._state = 0;

		doubleCard = false;
		doubleComb = false;
		_avoidClicks.SetActive (false);
    }


	private void notValidCombinationSelected()
    {
		AudioClip clip = Resources.Load<AudioClip> ("Audio/NotValidCombination");
		if (clip != null) {
			GetComponent <AudioSource> ().clip = clip;
			GetComponent <AudioSource> ().Play ();
		} else {
			Debug.Log ("AudioNotFound: " + "Audio/NotValidCombination");
		}

		GameObject.FindWithTag ("GUIManager").GetComponent<GUIManager> ().showAlertMessage ();
        resetStates();
	} // noValidCombinationSelected

    private bool RCTip = true;
    public void CardUnClicked()
    {
        if (RCTip)
        {
            RCTip = false;
            GameObject.Find("TextRightClickTiptxt").GetComponent<Text>().text = "";
        }
        if (current_clicked_cards == 1)
        {
            selected_element_1.GetComponent<CardHover>()._state = 0;
            current_clicked_cards = 0;
            Debug.Log("GAME_MANAGER: Card Counter Reset.");
        }
		_avoidClicks.SetActive (false);
    }   

    //compares the two selected elements. Returns true if they are a valid match.
    bool IsValidSelection()
    {

        string card_name_1 = selected_element_1.GetComponent<CardDetails>().CardName;
        string card_name_2 = selected_element_2.GetComponent<CardDetails>().CardName;
        return GetComponent<CardDatabase>().isValidCombination(card_name_1, card_name_2);
    }


}
