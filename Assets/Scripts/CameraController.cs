﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public bool _isDown = false;

	[Header("AngleRotation")]
	public float _maxRotation;
	public float _minRotation;

	[Header("Speed")]
	public float _angleSpeed;

	[Header("Distance To Finish")]
	public float _distanceToFinish = 0.1f;

	[Header("Second Programing")]
	public bool _second;
	public AnimationCurve _curve;
	public float _duration;

	private Quaternion _initialRotation;
	private float _timeStamp;

	private bool _movingAngle;
	private Quaternion _destinyAngle;
	//private float _originAngle;

	public void moveDown(){
		_movingAngle = true;
		_destinyAngle = Quaternion.Euler (_maxRotation, 0, 0);

		_initialRotation = transform.rotation;
        _isDown = true;
	} // moveToMinRotation

	public void moveUp(){
		_movingAngle = true;
		_destinyAngle = Quaternion.Euler (_minRotation, 0, 0);

        _initialRotation = transform.rotation;
        _isDown = false;
	} // moveToMaxRotation

	void Update(){
		//checkMouseInBorder ();

		if (!_movingAngle)
			return;
		
		if (!_second)
			updateWithLerp ();
		else
			updateWithAnimationCurve ();
		

	} // Update

	private void checkMouseInBorder(){
		if (!_movingAngle && Camera.main.ScreenToViewportPoint (Input.mousePosition).y < 0.1f)
			moveDown ();

		if (!_movingAngle && Camera.main.ScreenToViewportPoint (Input.mousePosition).y > 0.9f)
			moveUp ();
	} // checkMouseInBorder

	public void updateWithLerp(){
		
		transform.rotation = Quaternion.Lerp (transform.rotation,_destinyAngle , _angleSpeed*Time.deltaTime);

		if (Quaternion.Angle(transform.rotation ,_destinyAngle) < _distanceToFinish )
			_movingAngle = false;
	} // updateWithLerp

	public void updateWithAnimationCurve(){
	
		_timeStamp += Time.deltaTime;

		transform.rotation = Quaternion.Lerp (_initialRotation,_destinyAngle, 
			_curve.Evaluate(_timeStamp/_duration));

		if(_timeStamp > _duration){
			_timeStamp = 0;
			_movingAngle = false;
		}
	} // updateWithAnimationCurve


}
