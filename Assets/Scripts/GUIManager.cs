﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour {

	public float _timeAlertMessage = 1.5f;

	private CardDatabase _cardDatabase;
	// Use this for initialization
	void Start () {
		hideText ();
		hideAlertMessage ();
		hideNewElementMessage ();

		GameObject go = GameObject.FindWithTag ("GM");
		if (go != null) {
			_cardDatabase = go.GetComponent<CardDatabase> ();
			InvokeRepeating ("checkTextCurrentElements", 0, 1);
		}
	}
	
	public void clickInStartGame(){
		SceneManager.LoadScene (1);
	} // clickInStartGame

	public void clickInExit(){
		Application.Quit ();
	} // clickInExit

	public void addCraftText(string leftText, string rightText){
		Transform go = transform.Find ("TextLeft/Text");
		if (go == null)
			return;
		
		Text temp = go.GetComponent<Text> ();
		if(temp != null) temp.text = leftText;
		temp = transform.Find ("TextRight/Text").GetComponent<Text> ();
		if(temp != null) temp.text = rightText;
	}

	public void addTopText(string topText){
		Transform go = transform.Find ("TextTop/Text");
		if (go == null)
			return;
		Text temp = go.GetComponent<Text> ();
		if(temp != null) temp.text = topText;
	} // addCraftText

	public void hideText(){
		addCraftText ("", "");
		addTopText ("");
	} // hideText

	public void showAlertMessage(){
		transform.Find ("AlertMessage/Text").GetComponent<Text> ().text = "YOU CANNOT MIX THIS ELEMENTS";
		Invoke ("hideAlertMessage", _timeAlertMessage);
	} // showAlertMessage

	public void hideAlertMessage(){
		Transform tr = transform.Find ("AlertMessage/Text");
		if (tr == null)
			return;
		tr.GetComponent<Text> ().text = "";
	} // hideAlertMessage

	public void showNewElementMessage(){
		transform.Find ("NewElementMessage/Text").GetComponent<Text> ().text = "NEW ELEMENT!!";
		Invoke ("hideNewElementMessage", _timeAlertMessage);
	} // hideAlertMessage

	public void hideNewElementMessage(){
		Transform tr = transform.Find ("NewElementMessage/Text");
		if (tr == null)
			return;
		tr.GetComponent<Text> ().text = "";
	} // hideAlertMessage



	// calculate current resources
	private void checkTextCurrentElements(){

		if (_cardDatabase == null)
			return;
		
		int total, unlocked;
		unlocked = 4;
		foreach(var data in _cardDatabase.getAllCardInfo ()){
			if(data.Value.unlock)
				++unlocked;
		}
		total = 4 + _cardDatabase.getAllCardInfo ().Count;

		string text = "Combinations Discovered    " + unlocked + " / " + total; 
		transform.Find ("NumCombinations/Text").GetComponent<Text>().text = text ;
	}
}
