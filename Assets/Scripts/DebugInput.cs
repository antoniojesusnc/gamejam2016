﻿using UnityEngine;
using System.Collections;

public class DebugInput : MonoBehaviour {

	private int tempInt;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			Camera.main.GetComponent<CameraController> ().moveUp ();
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			Camera.main.GetComponent<CameraController> ().moveDown ();
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Vector3 destiny = GameObject.Find ("DebugTransform").transform.position;
			GameObject.Find ("Card01").GetComponent<CardMovement> ().moveTo (destiny, -90);
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Vector3 destiny = GameObject.Find ("DebugTransform").transform.position + Vector3.right *5;
			GameObject.Find ("Card02").GetComponent<CardMovement> ().moveTo (destiny, -90);
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			Vector3 destiny = GameObject.FindGameObjectWithTag ("CardPosition1").transform.position;
			GameObject.Find ("Card01").GetComponent<CardMovement> ().moveTo (destiny, -25);
		}

		if (Input.GetKeyDown (KeyCode.X)) {
			Vector3 destiny = GameObject.FindGameObjectWithTag ("CardPosition2").transform.position;
			GameObject.Find ("Card02").GetComponent<CardMovement> ().moveTo (destiny, -25);
		}

		if (Input.GetKeyDown (KeyCode.Alpha9)) {
			string cardName = "FIRE_" +((++tempInt).ToString("##")) ;
			Vector3 destiny = GameObject.Find ("GameManager").GetComponent<CardAllocator> ().addCard (cardName);
			GameObject prefab = Resources.Load<GameObject> ("DebugSphere");
			GameObject go = Instantiate (prefab) as GameObject;
			go.name = cardName;
			go.transform.position = destiny;


		}



	}
}
