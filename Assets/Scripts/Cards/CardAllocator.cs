﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardAllocator : MonoBehaviour {

	public Transform _refPos;
	public int _numRows;
	public Dictionary<string, int> _positions;
	public float _boxSize;
	public float _width;
	public int _cardPerRows;

	// Use this for initialization
	void Start () {
		_cardPerRows = Mathf.CeilToInt( CardDatabase.NUMBER_OF_COMBINATION / (float)_numRows);
		_boxSize = _width / (float)_cardPerRows;

		_positions = new Dictionary<string, int> ();
	} // Start
	
	public Vector3 addCard(string cardName){
		if (_positions.ContainsKey (cardName)) {
			Debug.Log ("CardAllocator: card already in the list");
			return Vector3.zero;
		}
		int pos = Random.Range(0, CardDatabase.NUMBER_OF_COMBINATION);
		int i = 50;
		while(--i > 0 && _positions.ContainsValue(pos)){
			pos = Random.Range(0, CardDatabase.NUMBER_OF_COMBINATION);
		}
		if (i <= 0)
			Debug.LogError ("CardAllocator: Error, pos not found");

		_positions.Add (cardName, pos);
		return calculateWorldPos (pos);

	} // addCard

	private Vector3 calculateWorldPos(int pos){
		int rowPos = pos / _cardPerRows;
		int columnPos = pos % _cardPerRows;

        float posX = _refPos.position.x + columnPos * (_boxSize * 1.2f) + (rowPos % 2 != 0 ? -_boxSize * 0.5f : 0.0f);
        float posY = _refPos.position.y - rowPos * (_boxSize * 1.65f); ;
		return new Vector3(posX, posY, 0);
	}
}
