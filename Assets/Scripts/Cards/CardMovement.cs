﻿using UnityEngine;
using System.Collections;

public class CardMovement : MonoBehaviour {

	[Header("CardSpeed")]
	public float _speed;
	public float _speedRotation;


	public float _distanceToStop;

	[Header("SecondMethod")]
	public bool _secondMethod;
	public AnimationCurve _curve;
	public float _movementDuration;
	private float _timeStamp;
	private Vector3 _initialPos;
	private Quaternion _initialRotation;

	private Vector3 _destiny;
	private Quaternion _destinyRotation;
	private bool _moving;

	public void moveTo(Vector3 destiny, float destinyRotation){
        _timeStamp = 0;
		_destiny = destiny;
		_destinyRotation = Quaternion.Euler (destinyRotation, 0, 0);
		_moving = true;

		_initialPos = transform.position;
		_initialRotation = transform.rotation;

		GetComponent<CardClickable> ().setHalo (false);
	} // moveTo

	// Update is called once per frame
	void Update () {
		if (!_moving)
			return;

		if (!_secondMethod) {
			UpdateWithLerp ();
		} else {
			UpdateWithCurve ();
		}

	} // Update

	private void UpdateWithLerp(){
		transform.position = Vector3.Lerp (transform.position, _destiny, _speed*Time.deltaTime);
		transform.rotation = Quaternion.Lerp (transform.rotation, _destinyRotation, _speedRotation*Time.deltaTime);

		if (Vector3.Distance (transform.position, _destiny) <= _distanceToStop)
			_moving = false;
	} // UpdateWithLerp

	private void UpdateWithCurve(){
		_timeStamp += Time.deltaTime;
		transform.position = Vector3.Lerp(_initialPos, _destiny, _curve.Evaluate(_timeStamp/ _movementDuration));
		transform.rotation = Quaternion.Lerp(_initialRotation, _destinyRotation, _curve.Evaluate(_timeStamp/ _movementDuration));

		if (_timeStamp > _movementDuration) {
			_moving = false;
		}
	} // UpdateWithCurve
}
