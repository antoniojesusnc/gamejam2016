﻿using UnityEngine;
using System.Collections;

public class CardClickable : MonoBehaviour {

    private bool clicked;
    private bool hovering = false;
    private float counter = 0.0f;

    // Use this for initialization
    void Start()
    {
        clicked = false;
		setHalo (false);
    }

    // Update is called once per frame
    void Update()
    {
        if (hovering)
        {
            counter += Time.deltaTime;

            if (counter > 0.1f)
            {
                GetComponent<FloatingText>().addText(GetComponent<CardDetails>().CardName);
            }
        }
        else
        {
            if (counter > 0.0f)
            {
                GetComponent<FloatingText>().removeText();
                counter = 0.0f;
            }
        }
    }

    void OnMouseOver()
    {
        hovering = true;

        if (Input.GetMouseButtonUp(0))
        {
            clicked = true;

            Debug.Log("CARD: left click.");

            GameObject GM = GameObject.FindGameObjectWithTag("GM");

			if( GM.GetComponent<GameManagerScript>().current_clicked_cards == 0)
				setHalo (true);

            GM.GetComponent<GameManagerScript>().CardClicked(gameObject);

			AudioClip clip = Resources.Load<AudioClip> ("Audio/Selected");
			if (clip != null) {
				GetComponent <AudioSource> ().clip = clip;
				GetComponent <AudioSource> ().Play ();
			} else {
				Debug.Log ("AudioNotFound: " + "Audio/Selected");
			}
        }
        else if (Input.GetMouseButtonUp(1))
        {

			AudioClip clip = Resources.Load<AudioClip> ("Audio/cardUnclicked");
			if(clip != null){
				GetComponent <AudioSource> ().clip = clip;
				GetComponent <AudioSource> ().Play();
			} else {
				Debug.Log ("AudioNotFound: " + "Audio/cardUnclicked");
			}
            Debug.Log("CARD: Right click.");

            if (clicked == true)
            {
                GameObject GM = GameObject.FindGameObjectWithTag("GM");
                GM.GetComponent<GameManagerScript>().CardUnClicked();
				setHalo (false);
                clicked = false;
            }
        }
    }

    void OnMouseExit()
    {
        hovering = false;
    }

	public void setHalo(bool halo){
		transform.Find ("Halo").GetComponent<Light> ().enabled = halo;
	}
}
