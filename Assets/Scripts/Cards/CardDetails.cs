﻿using UnityEngine;
using System.Collections;

public class CardDetails : MonoBehaviour {

    public string   CardName;
    public string   Origin_1;
    public string   Origin_2;
    public bool     Unlocked;
    public Vector3  ShelfPosition;

	// Use this for initialization
	void Start () 
    {
		
		CardName = string.IsNullOrEmpty(CardName)?"Unspecified":CardName;
		Origin_1 = string.IsNullOrEmpty(Origin_1)?"Unspecified":Origin_1;
		Origin_2 = string.IsNullOrEmpty(Origin_2)?"Unspecified":Origin_2;
        Unlocked = false;

        //Set Front Art Material of Card
        Renderer renderer = GetComponentsInChildren<MeshRenderer>()[0];
        renderer.material = Resources.Load("ElementCards/Materials/CardFront_" + CardName) as Material;


        //Set Middle Art of Card
                 //renderer = GetComponentsInChildren<MeshRenderer>()[1];
        //renderer.material = Resources.Load("Materials/CardMiddle_FIRE_Temp") as Material;

        //Set Back Art Material of Card
                 renderer = GetComponentsInChildren<MeshRenderer>()[1];
        renderer.material = Resources.Load("Materials/CardBack") as Material;
     
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetInfo(CardInfo CI)
    {
        CardName = CI.name;
        Origin_1 = CI.element1;
        Origin_2 = CI.element2;
        Unlocked = true;
    }
}
