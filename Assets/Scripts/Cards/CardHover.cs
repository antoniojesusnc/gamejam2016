﻿using UnityEngine;
using System.Collections;

public class CardHover : MonoBehaviour {

    public int   _hoverSpeed  = 3   ;
    public float _hoverAmp    = 1.0f;
    public int   _state = STATE_IDLE;

    public static int STATE_IDLE = 0;
    public static int STATE_SELECTED = 1;


    private bool waitasec = true;

    public float counter = 0.0f;

    private Vector3 _startPos;
    private Vector3 _tempPos;

	// Use this for initialization
	void Start ()
    {
        Invoke("Wait", Random.Range(1.5F, 3.0F));
	}
	
	// Update is called once per frame


    void Update()
    {
        if (!waitasec && _state == STATE_IDLE)
        {
            counter += Time.deltaTime;
            //_tempPos = transform.position;
            _tempPos.y = _startPos.y + Mathf.Sin(counter) * _hoverAmp;
            transform.position = _tempPos;
        }
    }

    void Wait()
    {
        _startPos = GetComponent<CardDetails>().ShelfPosition;
        _tempPos = GetComponent<CardDetails>().ShelfPosition;
        waitasec = false;
    }

    public void CardBack()
    {
        counter = 0;
        waitasec = true;
        Invoke("Wait", Random.Range(1.5F, 3.0F));
    }
}
