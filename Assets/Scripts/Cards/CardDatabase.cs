﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CardDatabase : MonoBehaviour {

	public static int NUMBER_OF_COMBINATION = 0;

	public TextAsset _combinationFile;

	private Dictionary<KeyValuePair<string, string>, CardInfo> _combinationsInfo;
	// Use this for initialization
	void Awake () {
	
		string file = readFile ();

		//Debug.Log (file);
		parseAndAddToStorage(file);

		NUMBER_OF_COMBINATION = _combinationsInfo.Count + 4;
		foreach (var temp in _combinationsInfo) {
			Debug.Log (temp.Key + " "+temp.Value);
		}
	} // Start

	private string readFile(){
		return _combinationFile.text;
	} // readFile

	private void parseAndAddToStorage(string file){

		string elmn1 = "";
		string elmn2 = "";
		string result = "";
		bool isCombination = false;
		bool nextIsElement = false;

		_combinationsInfo = new Dictionary<KeyValuePair<string, string>, CardInfo> ();
		foreach (string element in file.Split (' ', '\r')) {
			//Debug.Log ("e" + element);
			if(string.IsNullOrEmpty(element))
				continue;
			if (element.Contains("\n")) {
				if (!isCombination && !nextIsElement)
					continue;
				
				var combinations = new KeyValuePair<string, string> (elmn1, elmn2);
				CardInfo info = new CardInfo ();
				info.name = result;
				info.element1 = elmn1;
				info.element2 = elmn2;
				info.unlock = false;

				_combinationsInfo.Add (combinations, info);

				result = elmn1 = elmn2 = "";
				isCombination = false;
				nextIsElement = false;

				if(element.Length > 2)
					elmn1 = element.Remove(0,1);
			}
			if (element == "+") {
				isCombination = true;
			} else if (element == "=") {
				nextIsElement = true;
			} else {
				if (nextIsElement) {
					result = element.Contains("\n")?element.Remove(0,1):element;
				} else if (isCombination) {
					elmn2 = element.Contains("\n")?element.Remove(0,1):element;
				} else {
					if(element .Length > 2)
						elmn1 = element.Contains("\n")?element.Remove(0,1):element;
				}
			}
		}
	} // parseAndAddToStorage

	public bool isValidCombination(string element1, string element2){
		/*
		foreach (var temp in _combinationsInfo) {
			if (temp.Key.Key == element1 && temp.Key.Value == element2 ||
			   temp.Key.Key == element2 && temp.Key.Value == element1)
				return true;
		}
		return false;
		*/
        Debug.Log("Ele1 = " + element1 + " ++++ Ele2 = " + element2 + " . ");
		bool result = _combinationsInfo.ContainsKey (new KeyValuePair<string, string> (element1, element2));
		if( !result) result =  _combinationsInfo.ContainsKey (new KeyValuePair<string, string> (element2, element1));
		return result;

	} // isValidCombination
	
	public CardInfo getCardInfo(string element1, string element2){
        var index = new KeyValuePair<string, string> (element1, element2);
        if (_combinationsInfo.ContainsKey(index))
        {
            return _combinationsInfo [index];
        }else{
            return _combinationsInfo[new KeyValuePair<string, string>(element2, element1)];
        }
		
	} // getCardInfo

	public void setUnlock(string element1, string element2, bool unlock){
		_combinationsInfo [new KeyValuePair<string, string> (element1, element2)].unlock = unlock;
	} // setUnlock

	public Dictionary<KeyValuePair<string, string>, CardInfo> getAllCardInfo(){
		return _combinationsInfo;
	} // getAllCardInfo
}

public class CardInfo{
	public string name;
	public string element1;
	public string element2;
	public bool unlock;

	public override string ToString ()
	{
		return "name " + name +" element1" +element1+ " element2"+ element2 + " unlock" + unlock;
	}
}
