﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {

	public TextMesh _textMesh;

    private bool allText = false;

    void Start()
    {
        removeText();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!Camera.main.GetComponent<CameraController>()._isDown)
            {
                GameObject.Find("TextSpaceTiptxt").GetComponent<Text>().text = "";
                string myname = GetComponent<CardDetails>().CardName;
                addText(myname);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            removeText();
        }
        
    }

	public void addText(string text){//, Vector3 position){
		//_textMesh.transform.position = position;
		_textMesh.gameObject.SetActive (true);
		_textMesh.text = text;
	} // addText

	public void removeText(){
		_textMesh.gameObject.SetActive (false);
	}


}